import {writeFileSync, mkdirSync, existsSync} from 'fs';
import {dump} from 'js-yaml';

type Info = {
    title: string;
    version: string;
    description: string;
};
type Server = {
    url: string;
    desciption?: string;
}

type API = {
    openapi: string;
    info: Info;
    servers?: Server[];
};

type ResponseParam = {
    name: string,
    description?: string,
}

type AddRouteParams = {
    route: string,
    method: string,
    requestBody?: {[key: string]: any} | string,
    responses: {[key: string]: ResponseParam},
    callbacks?: {[key: string]: any} | string,
    tag: string,
    summary: string,
    description?: string,
    parameters?: {[key: string]: any},
    docPath?: string[],
};

type I18nKeys = {
    [key: string]: string;
};

type Translation = {
    [key: string]: I18nKeys;
};

type Tag = {
    description: string;
    name: string;
};

type Paths = {[key: string]: {
    [key: string]: any
}};

type GeneratedAPI = {
    paths: Paths,
    components: {
        schemas: {[key: string]: any},
    }
} & API;

export class OASGenerator {
    api: API = {
        openapi: '3.0.0',
        info: {
            title: 'Title',
            version: '1.0',
            description: '',
        },
    };
    tags: Tag[] = [];
    translation: Translation = {};

    paths: Paths = {};

    docPaths: Map<string[], Set<string>> = new Map();
    schemas: {[key: string]: any} = {};
    responses: {[key: string]: any} = {};

    constructor(title: string, description: string, version = '1.0', server?: Server) {
        this.api.info = {
            title,
            version,
            description,
        };

        if (server) {
            this.api.servers = [server];
        }

        this.docPaths.set([], new Set());
    }

    addTranslation(lang: string, key: string, text: string) {
        if (!this.translation[lang]) {
            this.translation[lang] = {};
        }
        this.translation[lang][key] = text;
    }

    generateOAS(
        apiPath: string,
    ) {
        if (!existsSync(apiPath)) {
            mkdirSync(apiPath);
        }

        Object.entries(this.translation).map(([lang, i18n]) => {
            if (!existsSync(`${apiPath}/${lang}`)) {
                mkdirSync(`${apiPath}/${lang}`);
            }

            const openAPIDeclaration:GeneratedAPI = {
                ...this.api,
                paths: Object.entries(this.paths)
                    .map(([name, schema]) => ([name, this.translateSchema(lang, schema)]))
                    .reduce((acc:any, [name, value]) => {
                        acc[name] = value;
                        return acc;
                    }, {}),
                components: {
                    schemas: Object.entries(this.schemas)
                        .map(([name, schema]) => ([name, this.translateSchema(lang, schema)]))
                        .reduce((acc:any, [name, value]) => {
                            acc[name] = value;
                            return acc;
                        }, {}),
                },
            };
            writeFileSync(
                `${apiPath}/${lang}/openapi.yaml`,
                dump(openAPIDeclaration),
            );
            if (!existsSync(`${apiPath}/${lang}/api-reference`)) {
                mkdirSync(`${apiPath}/${lang}/api-reference`);
            }
            writeFileSync(
                `${apiPath}/${lang}/api-reference/README.md`,
                `# ${i18n.apiReference || 'API'}`,
            );

            Object.entries(openAPIDeclaration.paths).map(([path, value]) => {
                const splitedPath = path.slice(1).split('/');
                if (splitedPath.length > 1) {
                    splitedPath.pop();
                    const newDir = splitedPath.join('/');
                    if (!existsSync(`${apiPath}/${lang}/api-reference/${newDir}`)) {
                        mkdirSync(`${apiPath}/${lang}/api-reference/${newDir}`, {recursive: true});
                        writeFileSync(
                            `${apiPath}/${lang}/api-reference/${newDir}/README.md`,
                            `# ${i18n[`${splitedPath[splitedPath.length - 1]}Dir`] || 'API'}`,
                        );
                    }
                }
                writeFileSync(
                    `${apiPath}/${lang}/api-reference${path}.md`,
                    Object.entries(value)
                        .map(([method, data]) =>
                            this.makeRouteMD(path, data.summary, method, data.description),
                        )
                        .reduce((acc, el) => acc.concat(el), ''),

                );
            });
        });
    }

    makeRouteMD(path: string, summary: string, method: string, description: string) {
        const splitedPath = path.slice(1).split('/');
        return `${description ? `---
description: ${description}
---` : ''}
# ${summary}

{% swagger src="${'../'.repeat(splitedPath.length)}openapi.yaml" path="${path}" method="${method}" %}
[openapi.yaml](${'../'.repeat(splitedPath.length)}openapi.yaml)
{% endswagger %}

`;
    }

    checkAndTranslateString(lang: string, value: string) {
        const translate = value.match('{{[^}]*}}');
        return translate ?
            this.translation[lang][translate[0].slice(2, -2)] :
            value;
    }

    translateSchema(lang: string, schema: {[key: string]: any}) {
        return Object.entries(schema).reduce((acc: any, [key, value]) => {
            let newValue = value;
            if (typeof newValue === 'string') {
                newValue = this.checkAndTranslateString(lang, newValue);
            }
            if (typeof newValue === 'object' && newValue && !newValue.length) {
                newValue = this.translateSchema(lang, newValue);
            }

            if (typeof newValue === 'object' && newValue && newValue.length) {
                newValue = newValue.map((el:any) => {
                    const result = typeof el === 'string' ?
                        this.checkAndTranslateString(lang, el) :
                        this.translateSchema(lang, el);
                    return result;
                });
            }

            acc[key] = newValue;
            return acc;
        }
        , {});
    }

    addSchema(name: string, properties: any, required?: string[]) {
        if (this.schemas[name]) {
            return false;
        }
        this.schemas[name] = {
            type: 'object',
            properties,
            required,
        };

        return true;
    }

    makeResponse(name: string, description?: string) {
        return {
            description,
            content: {
                'application/json': {
                    schema: {
                        $ref: `#/components/schemas/${name}`,
                    },
                },
            },
        };
    }

    makeRequestBody(name: string, description?: string, required?: string[]) {
        return {
            description,
            required,
            content: {
                'application/json': {
                    schema: {
                        $ref: `#/components/schemas/${name}`,
                    },
                },
            },
        };
    }

    addToc(name: string, description: string) {
        this.tags.push({name, description});
    }

    addRoute(params: AddRouteParams) {
        const responses = Object.keys(params.responses).reduce((acc: {[key: string]: any}, code) => {
            acc[code] = this.makeResponse(params.responses[code].name, params.responses[code].description);
            return acc;
        }, {});

        const docPathParam = params.docPath ?? [];

        const docPath = this.docPaths.get(docPathParam);

        if (!docPath) {
            this.docPaths.set(docPathParam, new Set([params.route]));
        } else {
            docPath.add(params.route);
        }

        const requestBody = typeof params.requestBody === 'string' ?
            this.makeRequestBody(params.requestBody) :
            params.requestBody;

        if (!this.paths[params.route]) {
            this.paths[params.route] = {};
        }

        this.paths[params.route][params.method] = {
            tags: [params.tag],
            security: [],
            summary: params.summary,
            description: params.description,
            responses,
            requestBody,
            callbacks: params.callbacks,
            parameters: params.parameters,
        };
    }
}
