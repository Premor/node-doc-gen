import {readFileSync} from 'fs';
import {OASGenerator} from '.';
test('generate open api yaml', async () => {
    const generator = new OASGenerator('qweEWQ', 'main description');

    generator.addTranslation('en', 'TestSignatureSummary', 'Check signature x-api-signature');
    generator.addTranslation('ru', 'TestSignatureSummary', 'Проверка корректности подписи x-api-signature');

    generator.addTranslation('en',
        'TestSignatureDescription',
        'You can test your signature in x-api-signature within this method.');
    generator.addTranslation('ru',
        'TestSignatureDescription',
        'Метод позволяет проверить корректность подписи x-api-signature');

    generator.addTranslation('en', 'ErrorsDescription', 'Errors array');
    generator.addTranslation('ru', 'ErrorsDescription', 'Массив ошибок');

    generator.addTranslation('en', 'test-signatureDir', 'test dir');
    generator.addTranslation('ru', 'test-signatureDir', 'тестовая дирректория');


    generator.addSchema('TestSignatureResponse', {
        success: {
            type: 'boolean',
            example: true,
        },
        response: {
            type: 'object',
            properties: {
                errors: {
                    description: '{{ErrorsDescription}}',
                    type: 'array',
                    items: {
                        type: 'string',
                        example: 'Signature check failed',
                    },
                    example: [],
                },
                checkSignatureResult: {
                    type: 'boolean',
                    example: true,
                },
                rightSignature: {
                    type: 'string',
                    example:
                        '69e377f1f8759e9810ce3a4c033c87036cd9affaf3ef68ad43799a425ce88da2',
                },
                signature: {
                    type: 'string',
                    example:
                        '69e377f1f8759e9810ce3a4c033c87036cd9affaf3ef68ad43799a425ce88da2',
                },
                receivedBody: {
                    type: 'string',
                    example: '{"nonce":1643881363781}',
                },
            },
        },
    });
    generator.addSchema('TestSignatureParams', {
        test1: {
            type: 'string',
            example: 'test 1',
        },
        test2: {
            type: 'object',
            properties: {
                qwe: {
                    type: 'string',
                    example: 'qweewqewqewq',
                },
                ewq: {
                    type: 'boolean',
                    example: true,
                },
            },
            required: ['qwe'],
        },
    }, ['test1']);

    generator.addTranslation('en', 'x-api-public-key', 'public key');
    generator.addTranslation('ru', 'x-api-public-key', 'публичный ключ');

    generator.addTranslation('en', 'x-api-signature', 'signature');
    generator.addTranslation('ru', 'x-api-signature', 'сигнатура');

    generator.addTranslation('en', 'Content-type', 'content type');
    generator.addTranslation('ru', 'Content-type', 'тип контента');

    const standardHeaders = [
        {
            in: 'header',
            description: '{{x-api-public-key}}',
            name: 'x-api-public-key',
            schema: {
                type: 'string',
            },
            required: true,
        },
        {
            in: 'header',
            description: '{{x-api-signature}}',
            name: 'x-api-signature',
            schema: {
                type: 'string',
            },
            required: true,
        },
        {
            in: 'header',
            description: '{{Content-type}}',
            name: 'Content-type',
            schema: {
                type: 'string',
                example: 'application/json',
            },
            required: true,
        },
    ];

    generator.addRoute({
        route: '/test-signature',
        method: 'post',
        summary: '{{TestSignatureSummary}}',
        description: '{{TestSignatureDescription}}',
        responses: {
            '200': {
                name: 'TestSignatureResponse',
            },
        },
        parameters: standardHeaders,
        requestBody: 'TestSignatureParams',
        tag: 'test-signature',
    });
    generator.addRoute({
        route: '/test-signature2',
        method: 'post',
        summary: 'Проверка корректности подписи x-api-signature t2',
        responses: {
            '200': {
                name: 'TestSignatureResponse',
            },
        },
        tag: 'test-signature',
    });
    generator.addRoute({
        route: '/test-signature/2',
        method: 'post',
        summary: 'Проверка корректности подписи x-api-signature t3',
        responses: {
            '200': {
                name: 'TestSignatureResponse',
            },
        },
        tag: 'test-signature',
    });
    await generator.generateOAS(`${__dirname}/built`);

    const api = readFileSync(`${__dirname}/built/en/openapi.yaml`).toString();
    const apiResult = readFileSync(`${__dirname}/test-result.yaml`).toString();
    expect(api).toBe(apiResult);
});
